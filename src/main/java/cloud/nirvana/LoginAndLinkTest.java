/*
 * Created By :Pramod
 * Class Details: Class runs for valid login details(Hotmail) and checks the Header links and tabs under the header and shows the links are working or not with output
 * Reviewed By:Rakesh
 */
package cloud.nirvana;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import cloud.nirvana.Props;
//class
public class LoginAndLinkTest
{
	 public static Properties props;
	
	 private static final Properties PROPERTIES = Props.getPropertiesInstance();
	 
	 public static String Link = PROPERTIES.getProperty("url");
	 public static String HeaderLink = PROPERTIES.getProperty("Link");
	 
	 public static String NetworkLink = PROPERTIES.getProperty("NetworkpageLink");
	 public static String StorageLink = PROPERTIES.getProperty("StoragepageLink");
	 public static String ServerLink = PROPERTIES.getProperty("ServerpageLink");
	 public static String CloudLink = PROPERTIES.getProperty("CloudpageLink");
	 public static String company = PROPERTIES.getProperty("Companyname");
	 public static String username = PROPERTIES.getProperty("Username");
	 public static String password = PROPERTIES.getProperty("Password");
	
 public static void main(String[] args) throws InterruptedException{

	 try{ 
	//add comment to webdriver and remove comment from htmlunitdriver for not displays the browser and run teh tests.
		 
          WebDriver driver = new FirefoxDriver();
	    //HtmlUnitDriver driver = new HtmlUnitDriver();
		 driver.navigate().to(Link);  // Use navigate instead of driver.get()
		 WebElement companyname=driver.findElement(By.id("company"));
		 companyname.sendKeys(company);
		 WebElement username1=driver.findElement(By.id("username"));
		 username1.sendKeys(username);
		 WebElement password1=driver.findElement(By.id("password"));
		 password1.sendKeys(password);
	     WebElement signin = driver.findElement(By.xpath("//button[@type='submit']"));
	     signin.click();
         driver.get(HeaderLink);   
        
         List<String> link=new ArrayList<String>();
    	 link.add(NetworkLink);
    	 link.add(ServerLink);
    	 link.add(StorageLink);
    	 link.add(CloudLink);
    	
    	 
    	 for(int k=0;k<link.size();k++)
    	 {
    		  try{
    	//add comment to webdriver and remove comment from htmlunitdriver for not displays the browser and run teh tests.
    					 
    			          WebDriver driver1 = new FirefoxDriver();
    				   // HtmlUnitDriver driver1 = new HtmlUnitDriver();
    			 // Use navigate instead of driver.get()
    					 driver1.navigate().to(link.get(k)); 
    					 //Get The Header elements by using Xpath
    			  WebElement header= driver1.findElement(By.xpath("/html/body/div/div/div")); 
    			  if(link.get(k).contains("network")){
    			     System.out.println("Network Page Contains "+header.findElements(By.tagName("a")).size()+" Links") ;
    			  }
    			  if(link.get(k).contains("server")){
    				     System.out.println("Server Page Contains "+header.findElements(By.tagName("a")).size()+" Links") ;
    				  }
    			  if(link.get(k).contains("storage")){
    				     System.out.println("Storage Page Contains "+header.findElements(By.tagName("a")).size()+" Links") ;
    				  }
    			  if(link.get(k).contains("cloud")){
    				     System.out.println("Cloud Page Contains "+header.findElements(By.tagName("a")).size()+" Links") ;
    				  }
    			  int i = header.findElements(By.tagName("a")).size(); //Get number of links
    			      //create loop based upon number of links to traverse all links 
    			  for(int j = 0;j<i;j++){  
    				  // We are re-creating "footer" webelement as DOM changes after navigate.back()  
    			   header= driver1.findElement(By.xpath("/html/body/div/div/div"));  
    			   header.findElements(By.tagName("a")).get(j).getText();
    			   header.findElements(By.tagName("a")).get(j).click();
    			      Thread.sleep(3000);
    			   System.out.println(driver1.getCurrentUrl());
    			   
    			   if(driver.getTitle().contains("404")) 
    			   {
    			       System.out.println("404 Found");
    			       return;
    			    }	
    			   
    			  }
    			  driver1.close();
    			  
    		      Thread.sleep(4000);
    			  }
    		 
    		  catch(Exception e)
 			 {
 				System.out.println("Title is not added into the page");
 			 }
 	 }
     
     driver.close();

   }
	 catch(Exception e)
	 {
		 
	 }
}
}