/*
 * Created By:Pramod
 * Class Details: This class is used to get the values from text.property file.
 * Reviewed By:Vishwas
 */



package cloud.nirvana;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import cloud.nirvana.*;
public class Props {
	private static final Properties PROPERTIES = new Properties();
	private static final File FILE = new File("test.properties");
	private Props()
	{
	
	}
	

		public static  Properties getPropertiesInstance(){
			
			FileInputStream fis = null;
			try {
			   //	System.out.println("File"+ FILE);
				
								fis = new FileInputStream(FILE);
				//System.out.println(" PROPERTIES instance is "+PROPERTIES+ " and file instance is "+FILE+" and fis instance is "+fis);
				PROPERTIES.load(fis);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(fis!=null){
					try {
						fis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			return PROPERTIES;
		}
	}